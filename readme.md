#VentureOak VOX

A Linux Containers manager created for Ventureoak's developers.

##Requirements

This tool requires lxc (Linux Containers package) and composer.

### Install LXC

```
apt-get install lxc lxc-templates
```

##Installation

The installation process is simple and quick:

```
composer global require ventureoak/vox ~0.3
```
LXC requires root permissions to run, so you must allow root to use vox tool:

```
sudo ln -s ~/.composer/vendor/bin/vox /usr/local/bin
```

## Usage

### List container

To list all of your containers:

```
sudo vox container:list
```


### Download container

To get a list of the all available containers:

```
sudo vox download:list
```

To download a container:

```
sudo vox download:container <container-name> [--start]
```
The optional *--start* will start the container after the download is finished.
 
### Create your own container

To create your own container, use the following command:
```
sudo vox container:create <container-name> [--dist=ubuntu] [--release=trusty] [--arch=amd64]
```
You can see a list of available options for creation at the end of the page

### Configure a container

To configure a container:
```
sudo vox container:config <container-name> [--ip=10.0.3.x] [--share=/share]
```

### Start a container

To start a container:
```
sudo vox container:start <container-name>
```

### Stop a container

To stop a container:
```
sudo vox container:stop <container-name>
```

### Destroy a container

To destroy a container:
```
sudo vox container:destroy <container-name> [--force]
```
The *--force* option will stop the container if it's running.

### Access a container

You can access a container from one of two methods:

#### Console access
```
sudo vox container:terminal <container-name>
```

#### Over SSH
```
ssh root@10.0.3.x
```

### List all installable packages

To list all available installable packages:

```
sudo vox package:list
```

### Install a package

To install a package:

```
sudo vox package:install <container-name> <package-name>
```


### Creation options

The following options are available for creation:

| DIST    | RELEASE   | ARCH    |
|---------|-----------|---------|    
| centos  | 6	      | amd64   |   
| centos  | 6         | i386    |       
| centos  | 7         | amd64   |     
| debian  | jessie    | amd64   |  
| debian  | jessie    | armel   |  
| debian  | jessie    | armhf   |  
| debian  | jessie    | i386    |  
| debian  | sid       | amd64   |
| debian  | sid	      | armel   | 
| debian  | sid	      | armhf   |  
| debian  | sid	      | i386    |   
| debian  | squeeze	  | amd64   | 
| debian  | squeeze	  | armel   |  
| debian  | squeeze	  | i386    |   
| debian  | wheezy	  | amd64   |  
| debian  | wheezy	  | armel   |  
| debian  | wheezy	  | armhf   |  
| debian  | wheezy	  | i386    |   
| fedora  | 19	      | amd64   |
| fedora  | 19	      | armhf   |
| fedora  | 19	      | i386    |
| fedora  | 20	      | amd64   |
| fedora  | 20	      | armhf   |
| fedora  | 20	      | i386    |
| fedora  | 21	      | amd64   |
| fedora  | 21	      | armhf   |
| fedora  | 21	      | i386    |
| fedora  | 22	      | amd64   |
| fedora  | 22	      | armhf   |
| fedora  | 22	      | i386    |
| gentoo  | current	  | amd64   |
| gentoo  | current	  | armhf   |
| gentoo  | current	  | i386    |
| oracle  | 6.5	      | amd64   |
| oracle  | 6.5	      | i386    |
| plamo   | 5.x	      | amd64   |
| plamo	  | 5.x	      | i386    |
| ubuntu  | precise	  | amd64   |
| ubuntu  | precise	  | armel   |
| ubuntu  | precise	  | armhf   |
| ubuntu  | precise	  | i386    |
| ubuntu  | trusty	  | amd64   |
| ubuntu  | trusty	  | arm64   |
| ubuntu  | trusty	  | armhf   |
| ubuntu  | trusty	  | i386    |
| ubuntu  | trusty	  | ppc64el |
| ubuntu  | utopic	  | amd64   | 
| ubuntu  | utopic	  | arm64   |
| ubuntu  | utopic	  | armhf   |
| ubuntu  | utopic	  | i386    |
| ubuntu  | utopic	  | ppc64el |
| ubuntu  | vivid	  | amd64   |
| ubuntu  | vivid	  | arm64   |
| ubuntu  | vivid	  | armhf   |
| ubuntu  | vivid     | i386    |
| ubuntu  | vivid     | ppc64el |
| ubuntu  | wily      | amd64   |
| ubuntu  | wily      | arm64   |
| ubuntu  | wily      | armhf   |
| ubuntu  | wily      | i386    |
| ubuntu  | wily      | ppc64el |