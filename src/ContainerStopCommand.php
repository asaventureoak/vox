<?php

namespace Ventureoak\Vox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ContainerStopCommand extends Command {

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('container:stop')
            ->setDescription('Stops a Linux Container')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the container.')
            ->addOption('restart', 'r', InputOption::VALUE_NONE, 'Restarts the container.')
        ;

    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {

        $name = $input->getArgument('name');

        $output->writeln("<comment>Stopping $name container...</comment>");
        passthru('lxc-stop -n '.$name);

        if($input->getOption('restart')){
            $output->writeln("<comment>Restarting $name container...</comment>");
            passthru('lxc-start -n '.$name.' -d');
        }
        $output->writeln("<info>Done!</info>");

    }


}