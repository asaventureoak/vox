<?php

namespace Ventureoak\Vox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DownloadListCommand extends Command {

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('download:list')
            ->setDescription('Get\'s a list of available vox\'s to download.')
        ;

    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("<comment>Getting list...</comment>");
        $client = new \GuzzleHttp\Client();
        $res = $client->get('http://downloads.ventureoak.com/vox/vox.list');
        echo $res->getBody();
        $output->writeln("<info>Done!</info>");

    }


}