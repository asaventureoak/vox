<?php

namespace Ventureoak\Vox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ContainerCreateCommand extends Command {

    private $distribution = 'ubuntu';
    private $ubuntuRelease = 'xenial';
    private $debianRelease = 'jessie';
    private $architecture = 'amd64';

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('container:create')
            ->setDescription('Creates a Linux Container')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the container.')
            ->addOption('dist', 'd', InputOption::VALUE_OPTIONAL, 'The distribution name (ubuntu|debian). Default is ubuntu.')
            ->addOption('release', 'r', InputOption::VALUE_OPTIONAL, 'The distribution release name (trusty|jessie). Default is trusty for ubuntu and jessie for debian.')
            ->addOption('arch', 'a', InputOption::VALUE_OPTIONAL, 'The distribution architecture (i386|amd64). Default is amd64.')
        ;

    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {

        $name = $input->getArgument('name');

        $distribution = $this->distribution;
        $release = $this->ubuntuRelease;
        $architecture = $this->architecture;

        if($input->getOption('dist')){
            $distribution = $input->getOption('dist');
        }

        if($input->getOption('release')){
            $distribution = $input->getOption('release');
        }

        if($input->getOption('arch')){
            $distribution = $input->getOption('arch');
        }

        passthru('lxc-create -t download -n '.$name.' -- -d ' . $distribution . ' -r ' . $release . ' -a ' . $architecture);

        passthru('lxc-start -n '.$name. ' -d');

        sleep(5);

        $base = __DIR__.'/../scripts/base';
        passthru('lxc-attach -n '.$name.' < '.$base);
    }


}