<?php

namespace Ventureoak\Vox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ContainerCloneCommand extends Command {

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('container:clone')
            ->setDescription('Gets container info.')
            ->addArgument('orig', InputArgument::REQUIRED, 'The name of the original container.')
            ->addArgument('dest', InputArgument::REQUIRED, 'The name of the destination container.')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Stops the container if it is running.')
        ;

    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {

        $original = $input->getArgument('orig');
        $destination = $input->getArgument('dest');

        $output->writeln("<comment>Clonning $original into $destination...</comment>");
        if($input->getOption('force')){
            $output->writeln("<comment>Stopping $original...</comment>");
            passthru('lxc-stop -n '.$original);
        }
        passthru('lxc-clone '.$original.' '.$destination);

        $output->writeln("<info>Done!</info>");

    }


}