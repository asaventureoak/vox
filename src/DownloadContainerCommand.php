<?php

namespace Ventureoak\Vox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DownloadContainerCommand extends Command {

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('download:container')
            ->setDescription('Download\'s a vox from the server.')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the container.')
            ->addOption('start', 's', InputOption::VALUE_NONE, 'Starts the container after download.')
        ;

    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {

        $name = $input->getArgument('name');

        $user = exec('whoami');

        if($user == "root")
            $path = '/var/lib/lxc/';
        else
            $path = '~/.local/share/lxc';

        $output->writeln("<comment>Donwloading $name container...</comment>");
        passthru('rm /tmp/'.$name.'.vox*');
        passthru('wget http://downloads.ventureoak.com/vox/'.$name.'.vox -P /tmp');
        $output->writeln("<comment>Expanding $name container...</comment>");
        passthru('tar -zxf /tmp/'.$name.'.vox -C '.$path);
        passthru('rm /tmp/'.$name.'.vox');

        if($input->getOption('start')){
            $output->writeln("<comment>Starting $name container...</comment>");
            passthru('lxc-start -n '.$name. ' -d');
        }

        $output->writeln("<info>Done!</info>");
    }


}