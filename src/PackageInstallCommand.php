<?php

namespace Ventureoak\Vox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PackageInstallCommand extends Command {

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('package:install')
            ->setDescription('Creates a Linux Container')
            ->addArgument('container', InputArgument::REQUIRED, 'The name of the container.')
            ->addArgument('package', InputArgument::REQUIRED, 'The package name.')
        ;

    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('container');
        $package = $input->getArgument('package');

        $dir = __DIR__.'/../scripts/';

        $output->writeln("<comment>Installing $package on $name...</comment>");
        passthru('lxc-attach -n '.$name.' < '. $dir.$package);
        $output->writeln("<info>Done!</info>");

    }


}