<?php

namespace Ventureoak\Vox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ContainerInfoCommand extends Command {

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('container:info')
            ->setDescription('Gets container info.')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the container.')
        ;

    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {

        $name = $input->getArgument('name');

        $output->writeln("<comment>Getting $name info...</comment>");
        passthru('lxc-info -n '.$name);
        $output->writeln("<info>Done!</info>");

    }


}