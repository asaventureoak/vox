<?php

namespace Ventureoak\Vox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ContainerDestroyCommand extends Command {

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('container:destroy')
            ->setDescription('Destroys a Linux Container')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the container.')
            ->addOption('force', 'f', InputOption::VALUE_NONE, 'Forces stop if the machine is running.')
        ;

    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {

        $name = $input->getArgument('name');

        if($input->getOption('force')) {
            $output->writeln("<comment>Stopping $name container...</comment>");
            passthru('lxc-stop -n ' . $name);
        }

        $output->writeln("<comment>Destroying $name container...</comment>");
        passthru('lxc-destroy -n '.$name);
        $output->writeln("<info>Done!</info>");

    }


}