<?php

namespace Ventureoak\Vox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ContainerTerminalCommand extends Command {

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('container:terminal')
            ->setDescription('Connects to a Linux Container terminal')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the container.')
        ;

    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {

        $name = $input->getArgument('name');

        $output->writeln("<comment>Accessing terminal on $name...</comment>");
        passthru('lxc-console -n '.$name);
        $output->writeln("<info>Done!</info>");

    }


}