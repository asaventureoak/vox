<?php

namespace Ventureoak\Vox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ContainerConfigCommand extends Command {

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('container:config')
            ->setDescription('Configures a linux container')
            ->addArgument('name', InputArgument::REQUIRED, 'The container name.')
            ->addOption('ip', null, InputOption::VALUE_OPTIONAL, 'The container IP address (10.0.3.x).')
            ->addOption('share', null, InputOption::VALUE_OPTIONAL, 'The host computer folder to share. It will be available on /host in the container.')
        ;

    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');

        $user = exec('whoami');

        if($user == "root")
            $path = '/var/lib/lxc/'.$name;
        else
            $path = '~/.local/share/lxc/'.$name;

        if(!file_exists($path."/config.bck")){
            passthru('cp '.$path.'/config '.$path.'/config.bck');
        }else{
            passthru('cp '.$path.'/config.bck '.$path.'/config');
        }

        $ip = null;
        $share = null;

        if($input->getOption('ip')){
            $ip = $input->getOption('ip');
            passthru('echo "lxc.network.ipv4 = '.$ip.'/24" >> '.$path.'/config');
        }

        if($input->getOption('share')){
            $share = $input->getOption('share');
            passthru('chmod 7777 '.$share);
            passthru('echo "lxc.mount.entry = '.$share.' host none bind.ro 0.0" >> '.$path.'/config');
        }

        if($ip || $share){
            $output->writeln("<comment>Applying configuration...</comment>");
            passthru('lxc-stop -n '.$name);
            passthru('lxc-start -n '.$name.' -d');
        }

        $output->writeln("<info>Done!</info>");
    }


}