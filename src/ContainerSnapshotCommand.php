<?php

namespace Ventureoak\Vox;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ContainerSnapshotCommand extends Command {

    /**
     * Configure the command options.
     *
     * @return void
     */
    protected function configure()
    {
        $this
            ->setName('container:snapshot')
            ->setDescription('Gets container info.')
            ->addArgument('name', InputArgument::REQUIRED, 'The name of the original container.')
            ->addOption('create', 'c', InputOption::VALUE_NONE, 'Creates a snapshot.')
            ->addOption('list', 'l', InputOption::VALUE_NONE, 'List container snapshots.')
            ->addOption('restore', 'r', InputOption::VALUE_REQUIRED, 'Restores a container snapshot.')
            ->addOption('destroy', 'd', InputOption::VALUE_REQUIRED, 'Destroys a container snapshot.')
        ;

    }

    /**
     * Execute the command.
     *
     * @param  \Symfony\Component\Console\Input\InputInterface  $input
     * @param  \Symfony\Component\Console\Output\OutputInterface  $output
     * @return void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {

        $name = $input->getArgument('name');

        if($input->getOption('create')){
            $output->writeln("<comment>Creating a snapshot...</comment>");
            passthru('lxc-snapshot -n '.$name);
        }elseif($input->getOption('list')){
            $output->writeln("<comment>Getting snapshot list...</comment>");
            passthru('lxc-snapshot -n '.$name. ' --list');
        }elseif($input->getOption('restore')){
            $snap = $input->getOption('restore');
            $output->writeln("<comment>Restoring snapshot $snap...</comment>");
            passthru('lxc-snapshot -n '.$name. ' --restore='. $snap .' '. $name);
        }elseif($input->getOption('destroy')){
            $snap = $input->getOption('destroy');
            $output->writeln("<comment>Destroying snapshot $snap...</comment>");
            passthru('lxc-snapshot -n '.$name. ' --destroy='.$snap);
        }else{
            $output->writeln('<error>You must select a valid option. Add --help to the command</error>');
        }

        $output->writeln("<info>Done!</info>");

    }


}